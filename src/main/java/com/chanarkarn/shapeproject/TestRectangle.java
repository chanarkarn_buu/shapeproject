/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeproject;

/**
 *
 * @author A_R_T
 */
public class TestRectangle {

    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(3, 4);
        System.out.println("Area of rectangle(w = " + rectangle1.getW() + ") is " + rectangle1.recArea());
        System.out.println("Area of rectangle(l = " + rectangle1.getL() + ") is " + rectangle1.recArea());
        rectangle1.setW(4);
        rectangle1.setL(6);
        System.out.println("Area of rectangle(w = " + rectangle1.getW() + ") is " + rectangle1.recArea());
        System.out.println("Area of rectangle(l = " + rectangle1.getL() + ") is " + rectangle1.recArea());
        rectangle1.setW(0);
        rectangle1.setL(0);
        System.out.println("Area of rectangle(w = " + rectangle1.getW() + ") is " + rectangle1.recArea());
        System.out.println("Area of rectangle(l = " + rectangle1.getL() + ") is " + rectangle1.recArea());
    }

}
