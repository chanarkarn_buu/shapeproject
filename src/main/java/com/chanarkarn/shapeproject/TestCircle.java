/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeproject;

/**
 *
 * @author A_R_T
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle circlel = new Circle(3);
        System.out.println("Area of circlel(r= " + circlel.getR() + ") is " + circlel.calArea());
        circlel.setR(2); //circlel.r = 2;
        System.out.println("Area of circlel(r= " + circlel.getR() + ") is " + circlel.calArea());
        circlel.setR(0); //circlel.r = 0;
        System.out.println("Area of circlel(r= " + circlel.getR() + ") is " + circlel.calArea());
        System.out.println(circlel.toString());
        System.out.println(circlel);
    }
   
}
