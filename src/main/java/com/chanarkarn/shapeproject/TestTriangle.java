/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeproject;

/**
 *
 * @author A_R_T
 */
public class TestTriangle {

    public static void main(String[] args) {
        Triangle triangle1 = new Triangle(3, 4);
        System.out.println("Area of rectangle(b = " + triangle1.getB() + ") is " + triangle1.trgArea());
        System.out.println("Area of rectangle(h = " + triangle1.getH() + ") is " + triangle1.trgArea());
        triangle1.setB(2);
        triangle1.setH(3);
        System.out.println("Area of rectangle(b = " + triangle1.getB() + ") is " + triangle1.trgArea());
        System.out.println("Area of rectangle(h = " + triangle1.getH() + ") is " + triangle1.trgArea());
        triangle1.setB(0);
        triangle1.setH(0);
        System.out.println("Area of rectangle(b = " + triangle1.getB() + ") is " + triangle1.trgArea());
        System.out.println("Area of rectangle(h = " + triangle1.getH() + ") is " + triangle1.trgArea());
    }

}
