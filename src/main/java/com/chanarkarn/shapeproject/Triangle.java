/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeproject;

/**
 *
 * @author A_R_T
 */
public class Triangle {

    private double b, h;

    public Triangle(double b, double h) {
        this.b = b;
        this.h = h;
    }

    public double trgArea() {
        return b * h * 0.5;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        if (b <= 0) {
            System.out.println("Error: Radius must more than zero!!!!");
            return;

        }
        this.b = b;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        if (h <= 0) {

            return;

        }
        this.h = h;
    }

}
